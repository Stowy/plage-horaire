"use strict";

// Start a sunday (0)
const TIME_ZONES = [
	[
		{
			start: 0,
			end: 24,
			mult: 2,
		},
	],
	[
		{
			start: 0,
			end: 6,
			mult: 2,
		},
		{
			start: 6,
			end: 20,
			mult: 1,
		},
		{
			start: 20,
			end: 23,
			mult: 1.5,
		},
		{
			start: 23,
			end: 24,
			mult: 2,
		},
	],
	[
		{
			start: 0,
			end: 6,
			mult: 2,
		},
		{
			start: 6,
			end: 20,
			mult: 1,
		},
		{
			start: 20,
			end: 23,
			mult: 1.5,
		},
		{
			start: 23,
			end: 24,
			mult: 2,
		},
	],
	[
		{
			start: 0,
			end: 6,
			mult: 2,
		},
		{
			start: 6,
			end: 20,
			mult: 1,
		},
		{
			start: 20,
			end: 23,
			mult: 1.5,
		},
		{
			start: 23,
			end: 24,
			mult: 2,
		},
	],
	[
		{
			start: 0,
			end: 6,
			mult: 2,
		},
		{
			start: 6,
			end: 20,
			mult: 1,
		},
		{
			start: 20,
			end: 23,
			mult: 1.5,
		},
		{
			start: 23,
			end: 24,
			mult: 2,
		},
	],
	[
		{
			start: 0,
			end: 6,
			mult: 2,
		},
		{
			start: 6,
			end: 20,
			mult: 1,
		},
		{
			start: 20,
			end: 23,
			mult: 1.5,
		},
		{
			start: 23,
			end: 24,
			mult: 2,
		},
	],
	[
		{
			start: 0,
			end: 6,
			mult: 2,
		},
		{
			start: 6,
			end: 23,
			mult: 1.5,
		},
		{
			start: 23,
			end: 24,
			mult: 2,
		},
	],
];

function computeTimes() {
	const startInput = document.getElementById("start").value;
	const endInput = document.getElementById("end").value;

	let startTime = new Date(Date.parse(startInput));
	let endTime = new Date(Date.parse(endInput));

	const time = getTime(startTime, endTime);

	const timeString = secondsToHms(time);
	document.getElementById(
		"output"
	).innerHTML = `Le temps majoré est : ${timeString}`;
}

/**
 * Gets the majored time between start and end.
 * @param {Date} startTime The start date.
 * @param {Date} endTime The end date.
 * @returns The majored time between start and end.
 */
function getTime(startTime, endTime) {
	const isTwoDays = startTime.getDay() != endTime.getDay();

	if (!isTwoDays) {
		return (
			getOneDayTime(startTime, endTime, TIME_ZONES[startTime.getDay()]) -
			1
		);
	} else {
		let totalTime = 0;

		// Clone start and end times
		let clonedStart = new Date(startTime);
		let clonedEnd = new Date(endTime);

		// Set start at the end of day and end at the start of day
		clonedStart.setHours(23, 59, 59);
		clonedEnd.setHours(0, 0, 0, 0);

		let dayOneTime = getOneDayTime(
			startTime,
			clonedStart,
			TIME_ZONES[startTime.getDay()]
		);
		let dayTwoTime = getOneDayTime(
			clonedEnd,
			endTime,
			TIME_ZONES[endTime.getDay()]
		);

		totalTime += dayOneTime;
		totalTime += dayTwoTime;

		return totalTime;
	}
}

/**
 * Get the majored time for one day.
 * @param {Date} startTime
 * @param {Date} endTime
 * @param {any} timeRanges
 * @returns The majored time for the day.
 * @throws If the dates are not on the same day.
 */
function getOneDayTime(startTime, endTime, timeRanges) {
	// Check if they are on the same day
	if (!datesAreOnSameDay(startTime, endTime)) {
		throw "Dates are not on the same day.";
	}

	let totalTime = 0;

	for (let i in timeRanges) {
		let timeZone = timeRanges[i];

		// Check for holiday
		const isChristmas =
			startTime.getDate() == 25 && startTime.getMonth() == 11;
		const isNewYear = startTime.getDate() == 1 && startTime.getMonth() == 0;

		if (isChristmas || isNewYear) {
			timeZone = {
				start: 0,
				end: 24,
				mult: 2,
			};
		}

		const startHour = startTime.getHours();
		const endHour = endTime.getHours();

		// Check if start time is between start and end
		if (startHour >= timeZone.start && startHour < timeZone.end) {
			// Check if end time is above this range
			if (endHour >= timeZone.end) {
				// Add the time of the range
				let timeZoneEnd = new Date(startTime);
				timeZoneEnd.setHours(timeZone.end, 0, 0, 0);
				const diff = timeZoneEnd.getTime() - startTime.getTime();
				totalTime += (diff / 1000.0) * timeZone.mult;
				startTime = timeZoneEnd;
			} else {
				const diff = endTime.getTime() - startTime.getTime();
				totalTime += (diff / 1000.0 + 1) * timeZone.mult;
				return totalTime;
			}
		}
	}

	return totalTime;
}

/**
 * Check if the two dates are on the same day.
 * @param {Date} first The first date.
 * @param {Date} second The second date.
 * @returns True if the dates are on the same day.
 */
const datesAreOnSameDay = (first, second) =>
	first.getFullYear() === second.getFullYear() &&
	first.getMonth() === second.getMonth() &&
	first.getDate() === second.getDate();

function secondsToHms(d) {
	d = Number(d);
	var h = Math.floor(d / 3600);
	var m = Math.floor((d % 3600) / 60);

	var hDisplay = h > 0 ? h + "h" : "";
	var mDisplay = m > 0 ? m + "m" : "";
	return hDisplay + mDisplay;
}
